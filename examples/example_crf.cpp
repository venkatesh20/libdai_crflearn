/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */

#include <dai/alldai.h>
#include <crfparamlearn.h>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace std;
using namespace dai;

int main(int argc, char **argv) {
	string factorGraph, training, test, obsvar;
	int predict = 1;
	int optim = 2;
	float learningrate = 0.01;
	float tolerance = 1.0e-6;
	int nitereopch = 5;
	float l1reg = 1.0e-4;
	float l2reg = 1.0e-5;
	int numbpiter = 10;
	float bptolerance = 1.0e-9;
	int verbose = 0;

	try {

		po::options_description desc("Allowed options");
		desc.add_options()
		("help", "produce help message")
		("factorgraph", po::value<string>(), "set the factorgraph")
		("training", po::value<string>(), "set the training data")
		("obsvar", po::value<string>(), "set the observed variables")
		("test", po::value<string>(), "set the test data")
		//("predict", po::value<int>(), "set if the predictions need to be saved 1 - yes, 0 - no , default: 1")
		("optim", po::value<int>(), "set the optimization method to be used 1 - Gradient ascent, 2 - Stochastic gradient ascent, default: 2")
		("learningrate", po::value<float>(), "set the learning rate, default: 0.01")
		("tolerance", po::value<float>(), "set the tolerance, stopping criteria for gradient ascent, default: 1.0e-6")
		("nitereopch", po::value<int>(), "set the number of iterations or epochs, default: 5")
		("l1reg", po::value<float>(), "set the L1 regularization constant, default: 1.0e-4")
		("l2reg", po::value<float>(), "set the L1 regularization constant, default: 1.0e-5")
		("numbpiter", po::value<int>(), "set the number of iterations for belief propagation")
		("bptolerance", po::value<float>(), "set the tolerance for belief propagation, default: 1.0e-9")
		("verbose", po::value<int>(), "set the log level");

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help")) {
			cout << desc << "\n";
			return 1;
		}

		if (vm.count("factorgraph")) {
			factorGraph = vm["factorgraph"].as<string>();
			cout << "factorgraph was set to " << factorGraph << ".\n";
		} else {
			cout << "factorgraph was not set.\n";
		}

		if (vm.count("training")) {
			training = vm["training"].as<string>();
			cout << "training was set to " << training << ".\n";

		} else {
			cout << "training data was not set.\n";
		}

		if (vm.count("test")) {
			test = vm["test"].as<string>();
			cout << "test was set to " << test << ".\n";
		} else {
			cout << "test data was not set.\n";
		}

		if (vm.count("predict")) {
			cout << "predict was set to " << predict << ".\n";
		}

		if (vm.count("obsvar")) {
			obsvar = vm["obsvar"].as<string>();
			cout << "obsvar was set to " << obsvar << ".\n";
		} else {
			cout << "observed variables was not set.\n";
			cout << "Mandatory parameters are not set, exiting ......\n";
			return 1;
		}

		if (vm.count("optim")) {
			optim = vm["optim"].as<int>();
			cout << "optim was set to " << optim << ".\n";
		}

		if (vm.count("learningrate")) {
			learningrate = vm["learningrate"].as<float>();
			cout << "learning rate was set to " << learningrate << ".\n";
		}

		if (vm.count("tolerance")) {
			tolerance = vm["tolerance"].as<float>();
			cout << "tolerance rate was set to " << tolerance << ".\n";
		}

		if (vm.count("nitereopch")) {
			nitereopch = vm["nitereopch"].as<int>();
			cout << "number of iterations/epochs was set to " << nitereopch << ".\n";
		}

		if (vm.count("l1reg")) {
			l1reg = vm["l1reg"].as<float>();
			cout << "L1 regularization const was set to " << l1reg << ".\n";
		}

		if (vm.count("l2reg")) {
			l1reg = vm["l2reg"].as<float>();
			cout << "L2 regularization const was set to " << l2reg << ".\n";
		}

		if (vm.count("numbpiter")) {
			numbpiter = vm["numbpiter"].as<int>();
			cout << "number of iterations for BP was set to " << numbpiter << ".\n";
		}

		if (vm.count("bptolerance")) {
			bptolerance = vm["bptolerance"].as<float>();
			cout << "L2 regularization const was set to " << bptolerance << ".\n";
		}

		if (vm.count("verbose")) {
			verbose = vm["verbose"].as<int>();
			cout << "verbose was set to " << verbose << ".\n";
		}

		CRFParamLearn *learner = new CRFParamLearn(factorGraph, training, obsvar);

		if (optim == 1) {
			cerr << "initializing gradient ascent learner" << endl;
			learner->initGradientAscent(nitereopch, tolerance, learningrate, l1reg, l2reg, numbpiter, bptolerance, (size_t) verbose);
		} else {
			cerr << "initializing stochastic gradient ascent learner" << endl;
			learner->initStochasticGradientAscent(nitereopch, learningrate, l1reg, l2reg, numbpiter, bptolerance, (size_t) verbose);
		}

		if (verbose > size_t(0)) {
			learner->logConfig();
		}
		learner->learn();
		learner->storeLearnedParameters("learned_" + factorGraph);
		cerr << "The learned parameters are saved in the file: " << "learned_" + factorGraph << endl;
		learner->predict(test);
		return 0;
	} catch (exception& e) {
		cerr << "error: " << e.what() << "\n";
		return 1;
	} catch (...) {
		cerr << "Exception of unknown type!\n";
	}
}

