/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */

#include <vector>
#include <map>
#include <iostream>
#include <fstream>

#include <dai/alldai.h>
#include <dai/bp.h>
#include <boost/numeric/ublas/vector.hpp>
#include <crfparamlearn.h>

namespace dai {

using namespace dai;
using namespace std;

void Optimizer::fgToVector(FactorGraph &fg, la_Vector &theta) {

	theta.clear();
	size_t theta_index = 0;

	for (int i = 0; i < fg.nrFactors(); i++) {
		Factor factor = fg.factor(size_t(i));
		for (int j = 0; j < factor.nrStates(); j++) {
			theta(theta_index) = log(factor.get(size_t(j)));
			++theta_index;
		}
	}

}

void Optimizer::vectorToFg(FactorGraph &fg, la_Vector &theta) {

	size_t theta_index = 0;

	for (int i = 0; i < fg.nrFactors(); i++) {
		Factor factor = fg.factor((size_t) (i));
		for (int j = 0; j < factor.nrStates(); j++) {
			factor.set(size_t(j), exp(theta(theta_index)));
			++theta_index;
		}
		fg.setFactor((size_t) (i), factor);
	}

}

la_Vector Optimizer::collectSuffStats(Observation obs) {
	/*	Initialise a zero vector for storing sufficient statistics of
	 single observation*/
	la_Vector singleObsStats(_paramSize);
	for (int i = 0; i < _paramSize; i++) {
		singleObsStats(i) = 0.0;
	}
	/*
	 Loop through the factor graph, and for each factor that belongs to the
	 factor graph, identify which state is observed in the given observation
	 */
	for (int factorIndex = 0; factorIndex < crfFG->nrFactors(); factorIndex++) {
		Factor factor = crfFG->factor(size_t(factorIndex));
		VarSet factorVarset = factor.vars();
		// Create a new observation that contains variables of the current factor only
		Observation factorObs;
		pair<Observation::iterator, bool> ret;
		bool skipFactor;
		for (VarSet::const_iterator v = factorVarset.begin(); v != factorVarset.end(); v++) {
			Observation::iterator observationIterator = obs.find(*v);
			if (observationIterator == obs.end()) {
				/*If a variable in the current factor is missing in the current observation,
				 then we assume none of the states in the factor are observed, i.e we count
				 zero for all the states in the current factor*/
				skipFactor = true;
				continue;
			} else {
				ret = factorObs.insert(pair<Var, size_t>((*observationIterator).first, (*observationIterator).second));
			}
		}

		if (!skipFactor) {
			size_t observed_state = calcLinearState(factorVarset, factorObs);
			map<size_t, size_t>::iterator offsetMapIterator;
			offsetMapIterator = _factorOffset.find(size_t(factorIndex));
			size_t newIndex = (offsetMapIterator->second) + observed_state;
			singleObsStats(newIndex) = 1.0;
		}
	}

	return singleObsStats;
}

void Optimizer::initializeTheta() {

	theta = la_Vector(_paramSize);
//	fgToVector(*crfFG, theta);
	//Initialise theta to zero vector
	for (vector<int>::size_type i = 0; i != theta.size(); i++) {
		theta(i) = 0.0;
	}
	vectorToFg(*crfFG, theta);

}

Optimizer::Optimizer(FactorGraph & crfFG, Evidence & evidence, vector<Var> & observedVars, Real learningRate, Real l1_c, Real l2_c, int numbpiter,
		Real bptolerance, size_t verbose) :
		L1_C(l1_c), L2_C(l2_c), _learningRate(learningRate) {
	this->crfFG = &crfFG;
	this->evidence = &evidence;
	this->observedVars = &observedVars;

	// Set some constants
	opts.set("maxiter", (size_t) numbpiter); // Maximum number of iterations
	opts.set("tol", bptolerance); // Tolerance for convergence
	opts.set("verbose", verbose); // Verbosity (amount of output generated)
	_paramSize = 0;
	// initialize parameter size
	for (int i = 0; i < this->crfFG->nrFactors(); i++) {
		Factor factor = this->crfFG->factor((size_t) (i));
		_factorOffset.insert(pair<size_t, size_t>(i, _paramSize));
		_paramSize += factor.nrStates();
	}
	if (verbose > size_t(0)) {
		cerr << "factor offset contains:\n";
		for (map<size_t, size_t>::iterator it = _factorOffset.begin(); it != _factorOffset.end(); it++)
			cerr << (*it).first << " => " << (*it).second << endl;

		cerr << "Number of parameters:" << _paramSize << endl;
	}
}
StochasticGradientAscent::StochasticGradientAscent(FactorGraph & crfFG, Evidence & evidence, vector<Var> & observedVars, int epochs,
		Real learningRate, Real l1_c, Real l2_c, int numbpiter, Real bptolerance, size_t verbose) :
		num_epochs(epochs), Optimizer(crfFG, evidence, observedVars, learningRate, l1_c, l2_c, numbpiter, bptolerance, verbose) {
}

la_Vector Optimizer::getGradient(Evidence::const_iterator & e) {

	la_Vector suffStat = collectSuffStats((*e));
	la_Vector expectedStat(_paramSize);
	for (int i = 0; i < _paramSize; i++) {
		expectedStat(i) = 0.0;
	}

	FactorGraph observedFG = FactorGraph(*crfFG);
	for (vector<Var>::iterator obsIt = observedVars->begin(); obsIt != observedVars->end(); obsIt++) {
		observedFG.clamp(observedFG.findVar(*obsIt), e->find(*obsIt)->second);
	}
	BP bp((observedFG), opts("updates", string("SEQRND"))("logdomain", false));
	bp.init();
	bp.run();

	size_t theta_index = 0;
	for (int i = 0; i < (*crfFG).nrFactors(); i++) {
		Factor factor = bp.beliefF(size_t(i));
		for (int j = 0; j < factor.nrStates(); j++) {
			expectedStat(theta_index) = factor.get(size_t(j));
			++theta_index;
		}
	}

	la_Vector gradient = suffStat - expectedStat;
	return gradient;
}

la_Vector Optimizer::getL1Gradient(la_Vector vec) {
	la_Vector l1Grad(vec.size());
	for (vector<int>::size_type i = 0; i != vec.size(); i++) {
		if (vec(i) > 0) {
			l1Grad(i) = 1.0;
		} else if (vec(i) < 0) {
			l1Grad(i) = -1.0;
		} else if (vec(i) == 0) {
			l1Grad(i) = 0.0;
		}
	}
	return l1Grad;

}

la_Vector Optimizer::getL2Gradient(la_Vector vec) {
	return vec;
}

void StochasticGradientAscent::optimize() {
	/* read the structure of the graph, initialize theta vector to zero and factors to 1 */
	initializeTheta();
	for (int j = 0; j < num_epochs; j++) {
		for (Evidence::const_iterator e = evidence->begin(); e != evidence->end(); e++) {
			la_Vector gradient = getGradient(e);

			theta += _learningRate * ((gradient) + (L1_C * getL1Gradient(theta)) + (L2_C * getL2Gradient(theta)));
		}
	}
	vectorToFg(*crfFG, theta);
}

GradientAscent::GradientAscent(FactorGraph &crfFG, Evidence &evidence, vector<Var> &observedVars, int iterations, Real tolerance, Real learningRate,
		Real l1_c, Real l2_c, int numbpiter, Real bptolerance, size_t verbose) :
		num_iterations(iterations), opt_tolerance(tolerance), Optimizer(crfFG, evidence, observedVars, learningRate, l1_c, l2_c, numbpiter,
				bptolerance, verbose) {

}

void GradientAscent::optimize() {

	initializeTheta();
	la_Vector gradientSum(theta.size());

	for (int j = 0; j < num_iterations; j++) {

		// initialize gradient sum to zero before each iteration
		for (vector<int>::size_type i = 0; i != gradientSum.size(); i++) {
			gradientSum(i) = 0.0;
		}

		for (Evidence::const_iterator e = evidence->begin(); e != evidence->end(); ++e) {
			gradientSum += getGradient(e);
		}

		la_Vector old_theta = theta;
		la_Vector avgGradient = (gradientSum / evidence->nrSamples());
		theta += _learningRate * ((avgGradient) + (L1_C * getL1Gradient(theta)) + (L2_C * getL2Gradient(theta)));

		// check for termination
		if ((norm_2(old_theta - theta) / norm_2(theta)) < opt_tolerance) {
			break;
		}

	}

	vectorToFg(*crfFG, theta);

}

void CRFParamLearn::readGraph(string &graph) {
	_thetaFG.ReadFromFile(graph.c_str());
}

void CRFParamLearn::readEvidence(string &evidenceFile) {
	ifstream estream(evidenceFile.c_str());
	_evidence.addEvidenceTabFile(estream, _thetaFG);
}

void CRFParamLearn::configureObservedVars(string &obsVarsConfig) {
	map<string, Var> varMap;
	for (vector<Var>::const_iterator v = _thetaFG.vars().begin(); v != _thetaFG.vars().end(); v++) {
		stringstream s;
		s << v->label();
		varMap[s.str()] = *v;
	}

	ifstream is(obsVarsConfig.c_str());
	string line;
	getline(is, line);

	// Parse observed nodes
	vector<string> header_fields;
	header_fields = tokenizeString(line, true);
	vector<string>::const_iterator p_field = header_fields.begin();
	if (p_field == header_fields.end())
		DAI_THROWE(INVALID_EVIDENCE_FILE, "Empty observed Nodes configuration");

	for (; p_field != header_fields.end(); ++p_field) {
		map<string, Var>::iterator elem = varMap.find(*p_field);
		if (elem == varMap.end())
			DAI_THROWE(INVALID_EVIDENCE_FILE, "Variable " + *p_field + " not known");
		_observedVars.push_back(elem->second);
	}

}

void CRFParamLearn::initGradientAscent(int iterations, Real tolerance, Real learningRate, Real l1_c, Real l2_c, int numbpiter, Real bptolerance,
		size_t verbose) {
	_optimizer = new GradientAscent(_thetaFG, _evidence, _observedVars, iterations, tolerance, learningRate, l1_c, l2_c, numbpiter, bptolerance,
			verbose);
}

void CRFParamLearn::initStochasticGradientAscent(int epochs, Real learningRate, Real l1_c, Real l2_c, int numbpiter, Real bptolerance,
		size_t verbose) {
	_optimizer = new StochasticGradientAscent(_thetaFG, _evidence, _observedVars, epochs, learningRate, l1_c, l2_c, numbpiter, bptolerance, verbose);
}

void CRFParamLearn::learn() {
	_optimizer->optimize();
}

void CRFParamLearn::predict(string testSet) {
	_optimizer->predict(testSet);
}

void Optimizer::predict(string testSet) {
	Evidence testData;
	ifstream estream(testSet.c_str());
	testData.addEvidenceTabFile(estream, *crfFG);
	ofstream predictions;
	predictions.open(("predictons_" + testSet).c_str());

	for (size_t i = 0; i < crfFG->nrVars(); i++) {
		if (i < (crfFG->nrVars() - 1)) {
			predictions << crfFG->var(i) << "\t";
		} else {
			predictions << crfFG->var(i) << endl;
		}
	}

	for (Evidence::const_iterator e = testData.begin(); e != testData.end(); ++e) {

		FactorGraph observedFG = FactorGraph(*crfFG);
		for (vector<Var>::iterator obsIt = observedVars->begin(); obsIt != observedVars->end(); obsIt++) {
			observedFG.clamp(observedFG.findVar(*obsIt), e->find(*obsIt)->second);
		}
		BP bp(observedFG, opts("updates", string("SEQRND"))("logdomain", false)("inference", string("MAXPROD"))("damping", string("0.1")));
		bp.init();
		bp.run();
		vector<size_t> mpstate = bp.findMaximum();
		for (size_t i = 0; i < mpstate.size(); i++)
			if (i < (crfFG->nrVars() - 1)) {
				predictions << mpstate[i] << "\t";
			} else {
				predictions << mpstate[i] << endl;
			}

	}

	predictions.close();

}

void CRFParamLearn::storeLearnedParameters(string fileName) {
	_thetaFG.WriteToFile(fileName.c_str());
}

CRFParamLearn::CRFParamLearn(string graph, string &samples, string &obVars) {
	readGraph(graph);
	readEvidence(samples);
	configureObservedVars(obVars);

}

}
