/*  This file is part of libDAI - http://www.libdai.org/
 *
 *  Copyright (c) 2006-2011, The libDAI authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
 */

#include <vector>
#include <map>
#include <iostream>
#include <fstream>

#include <dai/alldai.h>
#include <boost/numeric/ublas/vector.hpp>

namespace dai {

typedef boost::numeric::ublas::vector<double> la_Vector;
typedef std::map<Var, size_t> Observation;

class Optimizer {

protected:
	la_Vector theta;
	la_Vector expectation;
	FactorGraph *crfFG;
	Evidence *evidence;
	std::vector<Var> *observedVars;
	PropertySet opts;
	std::map<std::size_t, std::size_t> _factorOffset;
	std::size_t _paramSize;
	Real _learningRate;
	Real L1_C;
	Real L2_C;
	la_Vector getGradient(Evidence::const_iterator & e);
	la_Vector getL1Gradient(la_Vector vec);
	la_Vector getL2Gradient(la_Vector vec);

public:
	virtual ~Optimizer() {
	}
	;

	virtual void optimize()=0;

	Optimizer() {
	}
	;
	Optimizer(FactorGraph &crfFG, Evidence &evidence, std::vector<Var> &observedVars, Real learningRate, Real l1_c, Real l2_c, int numbpiter,
			Real bptolerance, size_t verbose);
	void fgToVector(FactorGraph &fg, la_Vector &theta);
	void vectorToFg(FactorGraph &fg, la_Vector &theta);
	void initializeTheta();
	la_Vector collectSuffStats(Observation);
	void predict(std::string testSet);
};

class GradientAscent: public Optimizer {
private:
	int num_iterations;
	Real opt_tolerance;

public:
	GradientAscent() {
	}
	;
	GradientAscent(FactorGraph &crfFG, Evidence &evidence, std::vector<Var> &observedVars, int iterations, Real opt_tolerance, Real learningRate,
			Real l1_c, Real l2_c, int numbpiter, Real bptolerance, size_t verbose);
	virtual void optimize();

};

class StochasticGradientAscent: public Optimizer {
private:
	int num_epochs;

public:
	StochasticGradientAscent() {
	}
	;
	StochasticGradientAscent(FactorGraph &crfFG, Evidence &evidence, std::vector<Var> &observedVars, int epochs, Real learningRate, Real l1_c,
			Real l2_c, int numbpiter, Real bptolerance, size_t verbose);
	virtual void optimize();

};

class CRFParamLearn {
	FactorGraph _thetaFG;
	std::vector<Var> _observedVars;
	Evidence _evidence;
	Optimizer *_optimizer;

public:
	void readGraph(std::string &graph);
	void readEvidence(std::string &evidenceFile);
	void configureObservedVars(std::string &obsVarsConfig);
	CRFParamLearn(std::string graph, std::string &samples, std::string &obVars);
	void initGradientAscent(int iterations, Real tolerance, Real learningRate, Real l1_c, Real l2_c, int numbpiter, Real bptolerance, size_t verbose);
	void initStochasticGradientAscent(int epochs, Real learningRate, Real l1_c, Real l2_c, int numbpiter, Real bptolerance, size_t verbose);
	void learn();
	void predict(std::string testSet);
	void storeLearnedParameters(std::string fileName);

	void logConfig() {
		using namespace std;
		cerr << "Number of factors:" << _thetaFG.nrFactors() << endl;
		cerr << "Number of variables:" << _thetaFG.nrVars() << endl;
		cerr << "Number of samples in the evidence:" << _evidence.nrSamples() << endl;
		cerr << "Observed Variables:" << endl;
		for (std::vector<int>::size_type i = 0; i != _observedVars.size(); i++) {
			cerr << _observedVars[i].label() << "\t";
		}
		cerr << endl;
	}

};

}
