CRF Parameter learning using libDai
===================================

*Introduction*
------------

We have implemented parameter learning for arbitrary structured Conditional Random Fields (CRF), using the inference routine implemented by libDai library. CRFs are a class of graphical models which are used for structured prediction, image segmentation and multi-label prediction.


*Files*
-----

This tool is an extention of libDai inference library. There are only 3 files added in the example directory of libdai

1. crfparamlearn.h
2. crfparamlearn.cpp
3. example_crf.cpp

*Installation*
-------------
The file example_crf is a command line tool for CRF Parameter learning.

*  First build libdai, following the installation instructions of libdai
*  compile example_crf using the below command

		g++ -I "<libdai-include-directory>" -I "<libdai-examples-directory>" -L"<libdai-lib-directory>" crfparamlearn.h crfparamlearn.cpp example_crf.cpp -oexample_crf -ldai -lgmpxx -lgmp -lboost_program_options

 Below is how the compilation command looks for me

		g++ -I "/home/venki/libDai_repo/libdai_crflearn/include/" -I "/home/venki/libDai_repo/libdai_crflearn/examples" -L"/home/venki/libDai_repo/libdai_crflearn/lib/" crfparamlearn.h crfparamlearn.cpp example_crf.cpp -oexample_crf -ldai -lgmpxx -lgmp -lboost_program_options

* An executable called example_crf will be generated in the libdai/examples directory

*Usage*
-------

* to see the allowed options use the command --help

		venki@venki-Studio-1535:~/libDai_repo/libdai_crflearn/examples$ example_crf --help
		Allowed options:
		  --help                produce help message
		  --factorgraph arg     set the factorgraph
		  --training arg        set the training data
		  --obsvar arg          set the observed variables
		  --test arg            set the test data
		  --optim arg           set the optimization method to be used 1 - Gradient 
					ascent, 2 - Stochastic gradient ascent, default: 2
		  --learningrate arg    set the learning rate, default: 0.01
		  --tolerance arg       set the tolerance, stopping criteria for gradient 
					ascent, default: 1.0e-6
		  --nitereopch arg      set the number of iterations or epochs, default: 5
		  --l1reg arg           set the L1 regularization constant, default: 1.0e-4
		  --l2reg arg           set the L1 regularization constant, default: 1.0e-5
		  --numbpiter arg       set the number of iterations for belief propagation
		  --bptolerance arg     set the tolerance for belief propagation, default: 
				1.0e-9
		  --verbose arg         set the log level

*Mandatory parameters*
--------------------

### factorgraph
This file should follow the factor graph [file format defined by libdai](http://cs.ru.nl/~jorism/libDAI/doc/fileformats.html). This file is used only to represent the graphical structure to be learnt, the value of the parameters can be just zero. A sample input file for discretized iris data can be found [here](https://bitbucket.org/venkatesh20/libdai_crflearn/src/8a7dc085f136ea52b568fe5b8f98cdc2555fc5bc/examples/iris_pairwise2.fg?at=master)

The attribute descriptions are as follows
  
  * 0 - sepal length - discretized to 20 bins
  * 1 - sepal width - discretized to 20 bins
  * 2 - petal length - discretized to 20 bins
   * 3 - petal width - discretized to 20 bins

The class label is binarized in to three attributes one for each class

   * 4 -  Iris Setosa
   * 5 - Iris Versicolour
   * 6 - Iris Virginica

### training
This file should follow the Evidence file format described in the libDai file formats. The samples provided in this file will be used for the parameter learning. A sample file for the Iris training data can be found in the [link](https://bitbucket.org/venkatesh20/libdai_crflearn/src/8a7dc085f136ea52b568fe5b8f98cdc2555fc5bc/examples/iris_train.csv?at=master)

### obsvar
This file describes which attributes are always observed in the data. This is a simple tab separated value file. A sample file for iris data which describes the observed variable configuration can be found in the [link](https://bitbucket.org/venkatesh20/libdai_crflearn/src/8a7dc085f136ea52b568fe5b8f98cdc2555fc5bc/examples/iris_train.csv?at=master)

### test
This file should follow the Evidence file format described in the libDai file formats. The samples provided in this file will be used for testing the model. A sample file for the Iris training data can be found in the [link](https://bitbucket.org/venkatesh20/libdai_crflearn/src/8a7dc085f136/examples/iris_test_unlabelled.csv?at=master)

This file can contain only the values for the observed variables, the other variables will be predicted and saved in the output.

*Optional parameters*
-------------------
### optim
crfparamlearn currently supports 2 ways of optimization, gradient ascent and stochastic gradient ascent. Stochastic gradient ascent is the default option and recommended for large data sets.

### learningrate
This provides an option to specify learning rate for the optimization method. The default is 0.01. A rule of thumb is try smaller values for stochastic gradient ascent like 1.0e-3, 1.0-4 and for gradient ascent you could start from the range 1.0e-1 and so on. 

### tolerance
Gradient ascent, being a smooth method of optimization offers an simple stopping criterion if no improvement is being made in the learning, this sets the tolerance for the stopping criterion. If the improvement made in the model is lesser than this tolerance, then the learning is stopped. This options only applies if gradient ascent is used.  The default value for this option is 1.0e-6.

### nitereopch
specifies the number of iterations for gradient ascent and number of epochs ( one complete iteration on the training data set is one epoch). Default value is 5.

### l1reg and l2reg
By default crfparamlearn offers elastic net regularization, if you want to switch off the regularization, simply provide zero as values for these parameters. Similarly you could only turn on either L1 or L2 regularization only.

### numbpiter
Crfparamlearn uses belief propagation behind the scenes in each iteration of the parameter learning. This options allows you to specify the maximum allowed number of iterations for each belief propagation run.

### bptolerance
 sets the tolerance for belief propagation

### verbose
set the log level, 0 (zero) is default, setting to 1 or more displays log messages.


*Output*
------
When the example_crf is run, it gives 2 outputs

* **Model:** The parameter values for the specified graphical model, this is saved as a new file. The name of the file will be "learned_<input-factor-graph-filename>". Say for eg. if the input factor graph file name is **iris_pairwise2.fg** the learned parameters can be saved in the file **learned_iris_pairwise2.fg**. This factor graph is the learned model and can be individually used for inference.
* **Predictions:** The predictions for the test file will be saved as a new file. The name of the new file will be "predictions_<input-test-file-name>". Say for eg. if the input test file is **iris_test_unlabelled.csv**, the predictions file will be **predictons_iris_test_unlabelled.csv**

*SampleData*
----------
Sample data is provided in the below files in the examples directory

* iris_pairwise.fg, iris_pairwise2.fg
* iris_train.csv
* obs_iris.tsv
* iris_test_unlabelled.csv, iris_test_labelled.csv ( labeled test data for verification)

*Example Usage*
-------------


* with only Mandatory parameters

		venki@venki-Studio-1535:~/libDai_repo/libdai_crflearn/examples$ ./example_crf --factorgraph iris_pairwise2.fg --training iris_train.csv --obsvar obs_iris.tsv  --test iris_test_unlabelled.csv 
		factorgraph was set to iris_pairwise2.fg.
		training was set to iris_train.csv.
		test was set to iris_test_unlabelled.csv.
		obsvar was set to obs_iris.tsv.
		initializing stochastic gradient ascent learner
		The learned parameters are saved in the file: learned_iris_pairwise2.fg

* with optional parameters

		venki@venki-Studio-1535:~/libDai_repo/libdai_crflearn/examples$ ./example_crf --factorgraph iris_pairwise2.fg --training iris_train.csv --obsvar obs_iris.tsv  --test iris_test_unlabelled.csv --optim 1 --learningrate 0.25 --nitereopch 2 --l2reg 0 
		factorgraph was set to iris_pairwise2.fg.
		training was set to iris_train.csv.
		test was set to iris_test_unlabelled.csv.
		obsvar was set to obs_iris.tsv.
		optim was set to 1.
		learning rate was set to 0.25.
		number of iterations/epochs was set to 2.
		L2 regularization const was set to 1e-05.
		initializing gradient ascent learner
		The learned parameters are saved in the file: learned_iris_pairwise2.fg


####*Note*
 Currently we do not support parameter sharing in CRF, and we hope to support it in the future.

